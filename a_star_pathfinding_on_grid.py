from dataclasses import dataclass

import heapq

@dataclass(order=True)
class Node:
    heuristic: int = 0
    cost: int = -1
    x: int = -1
    y: int = -1
    obstacle: bool = False
    start_end: bool = False
    path: bool = False

    def __repr__(self):
        r_val =  f"{self.cost:2}" if not self.obstacle else " l"
        r_val = r_val if not self.start_end else " S"
        r_val = r_val if not self.path else "  "
        return r_val


def distance(node1: Node, node2: Node) -> int:
    return max(abs(node1.x - node2.x), abs(node2.y - node1.y))
    # return abs(node2.x - node1.x) + abs(node2.y - node1.y)

def get_neighbor_in_grid(grid, source : Node):
    neighbor_list = []
    for i in range(-1, 2):
        for j in range(-1, 2):
            if source.x + i > 0 and source.y + j > 0 and source.x + i < 18 and source.y + j < 40:
                if not(i == 0 and j == 0):
                    neighbor_list.append(grid[source.x + i][source.y + j])
    return neighbor_list

def a_star_path_finding(start_node: Node, end_node: Node, grid):
    #Creating a list that will be used as a queue
    closed_list = []

    # Creating priority queue
    open_list = []
    heapq.heapify(open_list)

    #Pushing start node in list
    heapq.heappush(open_list, start_node)

    while len(open_list) != 0:
        u = heapq.heappop(open_list)
        if u.x == end_node.x and u.y == end_node.y:
            return construct_path(u, grid)
        neighbor_list = get_neighbor_in_grid(grid, u)
        for v in neighbor_list:
            if not (v in closed_list or (v in open_list)):
                v.cost = u.cost + 1
                v.heuristic = v.cost + distance(v, end_node)
                heapq.heappush(open_list, v)
        closed_list.append(u)
    return None

def construct_path(node: Node, grid):
    path = []
    while node.cost != 0:
        neighbor = get_neighbor_in_grid(grid, node)
        neighbor = [i for i in neighbor if i.cost != -1]
        min_neighbor = min(neighbor, key=lambda x: x.cost)
        min_neighbor.path = True
        path.append(min_neighbor)
        node = min_neighbor
    return path


def get_directions(path):
    dir_path = []
    last_x = path[0].x
    last_y = path[0].y
    direct = [["NW", "N", "NE"],
              ["W",  "O",  "E"],
              ["SW", "S", "SE"]]
    for n in path:
        dir_x = (last_x - n.x) + 1
        dir_y = (last_y - n.y) + 1
        dir_path.append(direct[dir_x][dir_y])
        last_x = n.x
        last_y = n.y
    return list(reversed(dir_path))

def main():
    start_node = Node(x=10, y=4, cost=0, start_end=True)
    end_node = Node(x=14, y=25, start_end=True)

    grid = [[Node(x=j, y=i) for i in range(40)] for j in range(18)]
    grid[start_node.x][start_node.y] = start_node
    grid[end_node.x][end_node.y] = end_node

    [print(i) for i in grid]
    print()

    path = a_star_path_finding(start_node, end_node, grid)
    print()
    [print(i) for i in grid]
    print()
    print(path)
    direction = get_directions(path)
    print(direction)


if __name__ == "__main__":
    main()